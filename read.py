from pymongo import MongoClient

def main():
    # Connect to the MongoDB - Adjust the connection string as needed
    client = MongoClient("mongodb://localhost:27017/")
    
    # Select the 'students' database
    db = client['students']
     
    # Select the 'student_info' collection
    collection = db['student_info']
    
    # Retrieve and print all records from the collection
    print("Students Information:")
    for student in collection.find():
        print(student)

if __name__ == "__main__":
    main()
