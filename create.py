from pymongo import MongoClient

# Create a MongoClient to the running mongod instance
client = MongoClient("mongodb://localhost:27017/")


# Create or switch to the students database
db = client["students"]

# Create or switch to the student_info collection
collection = db["student_info"]


# Sample data
students_data = [
    {"name": "Alice", "age": 22, "major": "Biology"},
    {"name": "Bob", "age": 24, "major": "History"},
    {"name": "Charlie", "age": 23, "major": "Engineering"},
    {"name": "Bhargavram", "age": 22, "major": "Engineerings"},
    {"name": "Harsha", "age": 22, "major": "Biology"},
    {"name": "Surya Reddy", "age": 22, "major": "Engineering"}
]

# Insert data into the collection
collection.insert_many(students_data)